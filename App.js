import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View,Image } from 'react-native';
import LeftMenu from './components/LeftMenu'
import MainMenu from './components/MainMenu'
import BuyPanel from './components/BuyPanel'
import SellPanel from './components/SellPanel'
import ShortPanel from './components/ShortPanel'
import CloseShortPanel from './components/CloseShortPanel'
import BuyPicker from './components/BuyPicker'
import SellPicker from './components/SellPicker'

import {ImageButton} from 'react-native-image-button-text';

const Stack = createStackNavigator()

export default function App() {
  return (

    <View style={styles.container}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="CloseShortPanel">
          <Stack.Screen name="BuyPanel" component={BuyPanel} />
          <Stack.Screen name="ShortPanel" component={ShortPanel} />
          <Stack.Screen name="CloseShortPanel" component={CloseShortPanel} />

          <Stack.Screen name="SellPanel" component={SellPanel} />

          <Stack.Screen name="MainMenu" component={MainMenu} />

        </Stack.Navigator>
      </NavigationContainer>


      <StatusBar style="auto" />
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
});
