var convertDT = (dt) => {
  let d  = new Date(dt)
  return d.getDate()+'/'+d.getMonth()+'/'+d.getYear()+' '+d.getHours()+'.'+d.getMinutes()+'น.'
}

export default {convertDT}
