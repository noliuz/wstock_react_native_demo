import React, { Component } from 'react';
import { StyleSheet,View,Image,Text,TextInput,Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import BuyPicker from './BuyPicker'
import {ImageButton} from 'react-native-image-button-text';
import config from '../config.js'


export default class BuyPanel extends Component {

  state = {
      symbol:'',
      amount:'',
      price:'',
      stop_loss:'',
      target_price:'',
      buy_id:'0'
  }

  constructor() {
    super()

    this.updateBuyState = this.updateBuyState.bind(this)
  }

  updateBuyState(symbol,amount,price,stop_loss,target_price,buy_id) {
    this.setState({
      symbol:symbol,
      amount:amount+'',
      price:price+'',
      stop_loss:stop_loss+'',
      target_price:target_price+'',
      buy_id:buy_id
    })
  }

  clearTextInput() {
    this.setState({
      symbol:'',
      amount:'',
      price:'',
      stop_loss:'',
      target_price:'',
      buy_id:0
    })
  }

  deleteBuy() {
    try {
      let url = config.API_URL+'/buy/delete/'+this.state.buy_id
      fetch(url)
      this.clearTextInput()
      this.refs.BuyPicker.componentDidMount()

    } catch (err) {
      console.log("Error delete buy data-----------", err);
    }
  }

  render() {
    return(
      <View style={{flex:1}}>
        <View style={{flex:1,marginTop:20}}>
          <View style={{flexDirection:'row'}}>
            <BuyPicker updateBuyState={this.updateBuyState}
              ref='BuyPicker'
            />
            {
                (this.state.buy_id != 0)?
                <ImageButton width={40} height={40} text='Delete'
                  backgroundColor='#d00' textColor='white' fontSize={12} style={{marginBottom:10}}
                  onPress={() => {
                    this.deleteBuy()
                  }}
                />:null
            }
          </View>
          <View>
            <Text
              onPress={()=>{
                this.refs.BuyPicker.componentDidMount()
              }}
            >
              Symbol
            </Text>
            <TextInput
              style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
              value={this.state.symbol}
              onChangeText={(text) => this.setState({symbol:text})}
            />
          </View>
          <View style={{marginTop:20}}>
            <Text >Amount</Text>
            <TextInput
              style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
              value={this.state.amount}
              onChangeText={(text) => this.setState({amount:text})}
            />
          </View>

          <View style={{marginTop:20}}>
            <Text >Price</Text>
            <TextInput
              style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
              value={this.state.price}
              onChangeText={(text) => this.setState({price:text})}
            />
          </View>

          <View style={{marginTop:20}}>
            <Text >Stop Loss</Text>
            <TextInput
              style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
              value={this.state.stop_loss}
              onChangeText={(text) => this.setState({stop_loss:text})}
            />
          </View>

          <View style={{marginTop:20}}>
            <Text>Target Price</Text>
            <TextInput
              style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
              value={this.state.target_price}
              onChangeText={(text) => this.setState({target_price:text})}
            />
          </View>
          <View style={{marginTop:40}}>
            <Button style={{}} title='Save'
              onPress={()=>{
                let symbol = this.state.symbol
                let amount = this.state.amount
                let price = this.state.price
                let stop_loss = this.state.stop_loss
                let target_price = this.state.target_price

                if (symbol == '' || amount == '' || price == '' ||
                  stop_loss == '' || target_price == ''
                )
                  return

                //save
                //console.log('buy_id='+this.state.buy_id)
                if (this.state.buy_id == 0) { //create new buy
                  let url = config.API_URL
                  url += '/buy/create/'+this.state.symbol+'/'
                  url += this.state.amount+'/'
                  url += this.state.price+'/'
                  url += this.state.stop_loss+'/'
                  url += this.state.target_price
                  console.log(url)
                  //call API
                  try {
                    console.log(url)
                    fetch(url)
                    this.clearTextInput()
                    this.refs.BuyPicker.componentDidMount()
                  } catch (err) {
                    console.log("Error send buy data-----------", err);
                  }
                } else {//update buy
                  let url = config.API_URL+'/buy/update/'
                  url += this.state.buy_id+'/'
                  url += this.state.symbol+'/'
                  url += this.state.amount+'/'
                  url += this.state.price+'/'
                  url += this.state.stop_loss+'/'
                  url += this.state.target_price
                  console.log(url)
                  //call API
                  try {
                    console.log(url)
                    fetch(url)
                    this.clearTextInput()
                    this.refs.BuyPicker.componentDidMount()
                  } catch (err) {
                    console.log("Error send buy data-----------", err);
                  }
                }

              }}
            />
          </View>
        </View>
      </View>
    )
  }
}
