import React, { Component } from 'react';
import { StyleSheet,View,Image,Text,TextInput,Button,Picker } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {ImageButton} from 'react-native-image-button-text';
import config from '../config.js'
import GeneralFunc from '../GeneralFunc.js'


export default class ShortPanel extends Component {

  state = {
      symbol:'',
      amount:'',
      price:'',
      stop_loss:'',
      target_price:'',
      short_id:'0',

      shortList: [],
      shortPickerSelectedValue:'Create new SHORT',
      shortPickerselectedIndex:'0'

  }

  constructor() {
    super()


  }

  componentDidMount() {
    this.updateShortPicker()
  }

  async updateShortPicker() {
    let url = config.API_URL
    url += '/short/get/-1'
    //call API
    try {
      console.log('update short picker ',url)
      let call = await fetch(url)
      let callJson = await call.json()
      console.log(JSON.stringify(callJson))
      this.setState({shortList:callJson})

    } catch (err) {
      console.log("Error updateShortPicker data-----------", err);
    }
  }

  updateShortUI(symbol,amount,price,stop_loss,target_price,short_id) {
    this.setState({
      symbol:symbol,
      amount:amount+'',
      price:price+'',
      stop_loss:stop_loss+'',
      target_price:target_price+'',
      short_id:short_id
    })
  }

  clearTextInput() {
    this.setState({
      symbol:'',
      amount:'',
      price:'',
      stop_loss:'',
      target_price:'',
      short_id:0
    })
  }

  deleteShort() {
    try {
      let url = config.API_URL+'/short/delete/'+this.state.shortList[this.state.shortPickerselectedIndex-1].id
      fetch(url)
      this.clearTextInput()
      this.updateShortPicker()
    } catch (err) {
      console.log("Error delete short-----------", err);
    }
  }

  render() {
    return(
      <View style={{flex:1}}>
        <View style={{flex:1,marginTop:20}}>
          <View style={{flexDirection:'row'}}>
          <Picker
            style={{ height: 50, width: 300 }}
            selectedValue={this.state.shortPickerSelectedValue}
            onValueChange={(val,idx)=> {
              //console.log('val='+val,idx)
              this.setState({shortPickerSelectedValue:val,shortPickerselectedIndex:idx})
              if (idx == 0) {
                this.updateShortUI('','','','','','0')
              } else {
                console.log(this.state.buyList)
                let bl = this.state.shortList[idx-1]
                let symbol = bl.symbol
                let amount = bl.amount
                let price = bl.price
                let stop_loss = bl.stop_loss
                let target_price = bl.target_price
                let short_id = bl.id                /this.updateShortUI(symbol,amount,price,stop_loss,target_price,short_id)
              }
            }}
          >
            <Picker.Item label="Create new SHORT" value={-1} key={-1}/>
            {this.state.shortList.map((item, index) => {
              return (<Picker.Item label={item.symbol+' '+item.price+'฿ '+GeneralFunc.convertDT(item.updatedAt)} value={item.id} key={item.id}/>)
            })}
          </Picker>

            {
                (this.state.shortPickerselectedIndex != 0)?
                <ImageButton width={40} height={40} text='Delete'
                  backgroundColor='#d00' textColor='white' fontSize={12} style={{marginBottom:10}}
                  onPress={() => {
                    this.deleteShort()
                  }}
                />:null
            }
          </View>
          <View>
            <Text
              onPress={()=>{

              }}
            >
              Symbol
            </Text>
            <TextInput
              style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
              value={this.state.symbol}
              onChangeText={(text) => this.setState({symbol:text})}
            />
          </View>
          <View style={{marginTop:20}}>
            <Text >Amount</Text>
            <TextInput
              style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
              value={this.state.amount}
              onChangeText={(text) => this.setState({amount:text})}
            />
          </View>

          <View style={{marginTop:20}}>
            <Text >Price</Text>
            <TextInput
              style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
              value={this.state.price}
              onChangeText={(text) => this.setState({price:text})}
            />
          </View>

          <View style={{marginTop:20}}>
            <Text >Stop Loss</Text>
            <TextInput
              style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
              value={this.state.stop_loss}
              onChangeText={(text) => this.setState({stop_loss:text})}
            />
          </View>

          <View style={{marginTop:20}}>
            <Text>Target Price</Text>
            <TextInput
              style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
              value={this.state.target_price}
              onChangeText={(text) => this.setState({target_price:text})}
            />
          </View>
          <View style={{marginTop:40}}>
            <Button style={{}} title='Save'
              onPress={()=>{
                let symbol = this.state.symbol
                let amount = this.state.amount
                let price = this.state.price
                let stop_loss = this.state.stop_loss
                let target_price = this.state.target_price

                if (symbol == '' || amount == '' || price == '' ||
                  stop_loss == '' || target_price == ''
                )
                  return

                //save
                //console.log('buy_id='+this.state.buy_id)
                if (this.state.shortPickerselectedIndex == 0) { //create new short
                  let url = config.API_URL
                  url += '/short/create/'+this.state.symbol+'/'
                  url += this.state.amount+'/'
                  url += this.state.price+'/'
                  url += this.state.stop_loss+'/'
                  url += this.state.target_price
                  console.log(url)
                  //call API
                  try {
                    console.log(url)
                    fetch(url)
                    this.clearTextInput()
                    this.updateShortPicker()

                  } catch (err) {
                    console.log("Error create short -----------", err);
                  }
                } else {//update buy
                  let url = config.API_URL+'/short/update/'
                  url += this.state.shortList[this.state.shortPickerselectedIndex-1].id+'/'
                  url += this.state.symbol+'/'
                  url += this.state.amount+'/'
                  url += this.state.price+'/'
                  url += this.state.stop_loss+'/'
                  url += this.state.target_price
                  console.log(url)
                  //call API
                  try {
                    console.log(url)
                    fetch(url)
                    this.clearTextInput()
                    this.updateShortPicker()
                  } catch (err) {
                    console.log("Error send buy data-----------", err);
                  }
                }

              }}
            />
          </View>
        </View>
      </View>
    )
  }
}
