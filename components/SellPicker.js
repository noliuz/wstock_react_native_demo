import React, { Component } from 'react';
import {View,Text,TextInput,Button,Picker } from 'react-native';
import config from '../config.js'
import GeneralFunc from '../GeneralFunc.js'

export default class SellPicker extends Component {
  state = {
        sellList: [],
        selectedValue:'Create new SELL',
        selectedIndex:0
  }

  async componentDidMount() {
    try {
      let url = config.API_URL+'/sell/get/-1'
      console.log('SellPicker didMount',url)
      const sellCall = await fetch(url)
      const sells = await sellCall.json()
      this.setState({sellList: sells})

    } catch (err) {
      console.log("Error fetching data-----------", err);
    }
  }

  render() {
    return(
      <View style={{flex:1,marginBottom:20}}>
        <Picker
          style={{ height: 50, width: 300 }}
          selectedValue={this.state.selectedValue}
          onValueChange={(val,idx)=> {
            //console.log('val='+val,idx)
            this.setState({selectedValue:val,selectedIndex:idx})
            if (idx == 0) {
              this.props.updateSellPickerState(idx,val,0,-1)
            } else {
              //console.log(this.state.buyList)
              let price = this.state.sellList[idx-1].price
              let id = this.state.sellList[idx-1].id
              //console.log('price = '+price)
              this.props.updateSellPickerState(idx,val,price,id)
            }
          }}
        >
          <Picker.Item label="Create new SELL" value={0} key={0}/>
          {this.state.sellList.map((item, index) => {
            return (<Picker.Item label={item.buy.symbol+' '+item.price+'฿ '+GeneralFunc.convertDT(item.updatedAt)} value={item.id} key={item.id}/>)
          })}


        </Picker>
      </View>
    )
  }
}
