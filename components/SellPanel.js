import React, { Component } from 'react';
import { StyleSheet,View,Image,Text,TextInput,Button,Picker } from 'react-native';
import {ImageButton} from 'react-native-image-button-text';
import SellPicker from './SellPicker'
import config from '../config.js'
import GeneralFunc from '../GeneralFunc.js'


export default class SellPanel extends Component {
  state = {
        leftBuyList: [],
        buySelIdx:0,
        buySelVal:'',
        buyListCount:'',
        sellPrice:'0',
        sellPickerSelIdx:0,
        sellPickerSelVal:'Create new SELL',
        sellPickerId:0,


  }

  constructor() {
    super()

    this.updateSellPickerState = this.updateSellPickerState.bind(this)

  }

  componentDidMount() {
    //load buy that not sell list
    this.loadBuyList()
  }

  async loadBuyDataFromSellId() {
    try {

      let sellId = this.state.sellPickerId
      let sellPickerIdx = this.state.sellPickerSelIdx
      console.log('sellPickerIdx ',sellPickerIdx)

      if (sellPickerIdx == 0) {//selected Create new SELL
        if (this.state.leftBuyList.length > this.state.buyListCount) {
          let resArr = this.state.leftBuyList.slice(1)
          let sellVal = resArr[0].symbol+' '+resArr[0].price+'฿ '+GeneralFunc.convertDT(resArr[0].updatedAt)
          this.setState({leftBuyList:resArr,buySelIdx:0,buySelVal:sellVal})

        }
      } else {
        //get buy data from sell get
        let url = config.API_URL+'/sell/get/'+sellId
        console.log('get buy data from sell id:',url)
        const sellCall = await fetch(url)
        const sell = await sellCall.json()

        //delete 1 extra buy list
        let resArr
        if (this.state.leftBuyList.length > this.state.buyListCount) {
          resArr = this.state.leftBuyList.slice(1)
        } else {
          resArr = this.state.leftBuyList
        }

        //add buy from sell selecting
        resArr.splice(0,0,sell[0].buy)
        let selVal = resArr[0].symbol+' '+resArr[0].price+'฿ '+GeneralFunc.convertDT(resArr[0].updatedAt)
        this.setState({leftBuyList:resArr,buySelIdx:0,buySelVal:selVal})
      }

    } catch (err) {
      console.log("Error fetching data-----------", err);
    }
  }

  async loadBuyList() {
    try {
      let url = config.API_URL+'/buy_left_in_port_list'
      //console.log(url)
      const buyCall = await fetch(url)
      const buys = await buyCall.json()
      this.setState({leftBuyList: buys,buyListCount:buys.length})

      //console.log(JSON.stringify(buys))

    } catch (err) {
      console.log("Error fetching data-----------", err);
    }
  }

  updateSellPickerState(sellPickerSelIdx,sellPickerSelVal,sellPrice,
        sellPickerId) {
    this.setState({
      sellPickerSelIdx:sellPickerSelIdx,
      sellPickerSelVal:sellPickerSelVal,
      sellPrice:sellPrice,
      sellPickerId:sellPickerId
    },()=> {
      this.loadBuyDataFromSellId()
    })

  }

  createSell() {
    try {
      let leftBuyId = this.state.leftBuyList[this.state.buySelIdx].id
      let sellPrice = this.state.sellPrice
      let url = config.API_URL+'/sell/create/'+leftBuyId+'/'+sellPrice

      console.log(url)
      fetch(url).then(() =>{
        this.setState({sellPrice:'0'})
        this.refs.SellPicker.componentDidMount()
      })


    } catch (err) {
      console.log("Error delete buy data-----------", err);
    }
  }

  updateSell() {
    try {
      let sellId = this.state.sellPickerId
      let sellPrice = this.state.sellPrice
      let buyId = this.state.leftBuyList[this.state.buySelIdx].id
      let url = config.API_URL+'/sell/update/'+sellId+'/'+buyId+'/'+sellPrice

      console.log(url)

      fetch(url).then(() => {
        this.setState({sellPrice:'0'})
        this.refs.SellPicker.componentDidMount()
      })

    } catch (err) {
      console.log("Error update sell data-----------", err);
    }
  }

  deleteSell() {
    try {
      let sellId = this.state.sellPickerId
      let url = config.API_URL+'/sell/delete/'+sellId

      console.log('deleteSell ',url)

      fetch(url).then(() => {
        console.log('<<<<<<<<<<<<<<<<<<<<<<<<<')
        this.setState({sellPrice:'0'})
        this.refs.SellPicker.componentDidMount()
      })

    } catch (err) {
      console.log("Error delete sell data-----------", err);
    }
  }

  render() {
    return(
      <View style={{flex:1,marginTop:20}}>
        <View>
          <Text>Sell List </Text>
          <View style={{flexDirection:'row'}}>
            <SellPicker
              updateSellPickerState={this.updateSellPickerState}
              ref='SellPicker'
            />
            {
                (this.state.sellPickerSelIdx != 0)?
                <ImageButton width={40} height={40} text='Delete'
                  backgroundColor='#d00' textColor='white' fontSize={12} style={{marginBottom:10,marginRight:10}}
                  onPress={() => {
                    this.deleteSell()
                  }}
                />:null

            }
          </View>
          <Text style={{marginTop:30}}>Buy List </Text>
          <Picker
            style={{ height: 50, width: 250 }}
            selectedValue={this.state.buySelVal}
            onValueChange={(val,idx)=> {
              this.setState({buySelVal:val,buySelIdx:idx})
              //console.log(idx)
            }}
          >
            {
              this.state.leftBuyList.map((item, index) => {
                return (
                  <Picker.Item label={item.symbol+' '+item.price+'฿ '+GeneralFunc.convertDT(item.updatedAt)} value={item.symbol+' '+item.price+'฿ '+GeneralFunc.convertDT(item.updatedAt)} key={item.id}/>
                )
              })
            }

          </Picker>
        </View>
        <View style={{marginTop:10}} >
          <Text>Price</Text>
          <TextInput
            style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
            value={this.state.sellPrice+''}
            onChangeText={(text) => this.setState({sellPrice:text})}
            onFocus={()=>this.setState({sellPrice:''})}
          />
        </View>
        <View style={{marginTop:20,width:100}}>
          <Button style={{}}
            title='Save'
            onPress={()=> {
              if (this.state.sellPrice != 0 &&
                  this.state.sellPickerSelIdx == 0) {
                //create new sell
                this.createSell()
              }

              if (this.state.sellPickerSelIdx != 0) {//update sell
                let price = this.state.sellPrice
                let id = this.state.sellPickerId

                this.updateSell()
              }

              this.refs.SellPicker.componentDidMount()
              this.loadBuyList()

            }}
          />
        </View>
      </View>
    )
  }
}
