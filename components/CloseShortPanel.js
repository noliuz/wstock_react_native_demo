import React, { Component } from 'react';
import { StyleSheet,View,Image,Text,TextInput,Button,Picker } from 'react-native';
import {ImageButton} from 'react-native-image-button-text';
import config from '../config.js'
import GeneralFunc from '../GeneralFunc.js'


export default class SellPanel extends Component {
  state = {
        leftShortList: [],
        leftShortPickerSelectedIndex:0,
        leftShortPickerSelectedValue:'',
        leftShortPickerId:0,
        closeShortList:[],
        buySelIdx:0,
        buySelVal:'',
        buyListCount:'',
        closeShortPrice:'0',
        closeShortPickerSelectedIndex:0,
        closeShortPickerSelectedValue:'Create new CLOSE SHORT',
        closeShortPickerId:0,

  }

  constructor() {
    super()

  }

  componentDidMount() {
    //load buy that not sell list
    this.updateLeftShortPicker()
    this.updateCloseShortPicker()
  }

  async updateLeftShortPicker() {
    let url = config.API_URL
    url += '/short_left_in_port_list'
    //call API
    try {
      //console.log('update left short picker ',url)
      let call = await fetch(url)
      let callJson = await call.json()
      //console.log(JSON.stringify(callJson))
      this.setState({leftShortList:callJson})

    } catch (err) {
      console.log("Error updateLeftShortPicker -----------", err);
    }
  }

  async updateCloseShortPicker() {
    let url = config.API_URL
    url += '/close_short/get/-1'
    //call API
    try {
      //console.log('update close short picker ',url)
      let call = await fetch(url)
      let callJson = await call.json()
      //console.log(JSON.stringify(callJson))
      this.setState({closeShortList:callJson})

    } catch (err) {
      console.log("Error updateCloseShortPicker -----------", err);
    }
  }

  async loadBuyDataFromSellId() {
    try {

      let sellId = this.state.sellPickerId
      let sellPickerIdx = this.state.sellPickerSelIdx
      console.log('sellPickerIdx ',sellPickerIdx)

      if (sellPickerIdx == 0) {//selected Create new SELL
        if (this.state.leftBuyList.length > this.state.buyListCount) {
          let resArr = this.state.leftBuyList.slice(1)
          let sellVal = resArr[0].symbol+' '+resArr[0].price+'฿ '+GeneralFunc.convertDT(resArr[0].updatedAt)
          this.setState({leftBuyList:resArr,buySelIdx:0,buySelVal:sellVal})

        }
      } else {
        //get buy data from sell get
        let url = config.API_URL+'/sell/get/'+sellId
        console.log('get buy data from sell id:',url)
        const sellCall = await fetch(url)
        const sell = await sellCall.json()

        //delete 1 extra buy list
        let resArr
        if (this.state.leftBuyList.length > this.state.buyListCount) {
          resArr = this.state.leftBuyList.slice(1)
        } else {
          resArr = this.state.leftBuyList
        }

        //add buy from sell selecting
        resArr.splice(0,0,sell[0].buy)
        let selVal = resArr[0].symbol+' '+resArr[0].price+'฿ '+GeneralFunc.convertDT(resArr[0].updatedAt)
        this.setState({leftBuyList:resArr,buySelIdx:0,buySelVal:selVal})
      }

    } catch (err) {
      console.log("Error fetching data-----------", err);
    }
  }

  async loadBuyList() {
    try {
      let url = config.APaI_URL+'/buy_left_in_port_list'
      //console.log(url)
      const buyCall = await fetch(url)
      const buys = await buyCall.json()
      this.setState({leftBuyList: buys,buyListCount:buys.length})

      //console.log(JSON.stringify(buys))

    } catch (err) {
      console.log("Error fetching data-----------", err);
    }
  }

  updateCloseShortUI() {

  }

  updateSellPickerState(sellPickerSelIdx,sellPickerSelVal,sellPrice,
        sellPickerId) {
    this.setState({
      sellPickerSelIdx:sellPickerSelIdx,
      sellPickerSelVal:sellPickerSelVal,
      sellPrice:sellPrice,
      sellPickerId:sellPickerId
    },()=> {
      this.loadBuyDataFromSellId()
    })

  }

  createSell() {
    try {
      let leftBuyId = this.state.leftBuyList[this.state.buySelIdx].id
      let sellPrice = this.state.sellPrice
      let url = config.API_URL+'/sell/create/'+leftBuyId+'/'+sellPrice

      console.log(url)
      fetch(url).then(() =>{
        this.setState({sellPrice:'0'})
        this.refs.SellPicker.componentDidMount()
      })


    } catch (err) {
      console.log("Error delete buy data-----------", err);
    }
  }


  deleteSell() {
    try {
      let sellId = this.state.sellPickerId
      let url = config.API_URL+'/sell/delete/'+sellId

      console.log('deleteSell ',url)

      fetch(url).then(() => {
        console.log('<<<<<<<<<<<<<<<<<<<<<<<<<')
        this.setState({sellPrice:'0'})
        this.refs.SellPicker.componentDidMount()
      })

    } catch (err) {
      console.log("Error delete sell data-----------", err);
    }
  }

  render() {
    return(
      <View style={{flex:1,marginTop:20}}>
        <View>
          <Text>Close Short List </Text>
          <View style={{flexDirection:'row'}}>
            <Picker
              style={{ height: 50, width: 300 }}
              selectedValue={this.state.closeShortPickerSelectedValue}
              onValueChange={(val,idx)=> {
                console.log('val='+val,idx)
                //change selected value,index
                this.setState({closeShortPickerSelectedValue:val,closeShortPickerSelectedIndex:idx},()=>{
                  //console.log('xxx ',this.state.closeShortPickerSelectedIndex)
                  //add Left Short List
                  if (this.state.closeShortPickerSelectedIndex == 0)
                    return

                  console.log('...........................')
                  let newCloseShortList = this.state.closeShortList[this.state.closeShortPickerSelectedIndex-1]
                  let newLeftShortList = this.state.leftShortList
                  newLeftShortList.push(newCloseShortList)

                  this.setState({leftShortList:newLeftShortList})

                })


                /*
                if (idx == 0) {
                  //this.updateCloseShortUI('','','','','','0')
                } else {
                  console.log(this.state.buyList)
                  let bl = this.state.shortList[idx-1]
                  let symbol = bl.symbol
                  let amount = bl.amount
                  let price = bl.price
                  let stop_loss = bl.stop_loss
                  let target_price = bl.target_price
                  let short_id = bl.id
                  ///this.updateCloseShortUI(symbol,amount,price,stop_loss,target_price,short_id)
                }*/
              }}
            >
              <Picker.Item label="Create new CLOSE SHORT" value={-1} key={-1}/>
              {this.state.closeShortList.map((item, index) => {
                return (<Picker.Item label={item.short_sell.symbol+' '+item.price+'฿ '+GeneralFunc.convertDT(item.updatedAt)} value={item.id} key={item.id}/>)
              })}
            </Picker>
          </View>
          <Text style={{marginTop:30}}>Left Short List </Text>
          <View style={{flexDirection:'row'}}>
            <Picker
              style={{ height: 50, width: 300 }}
              selectedValue={this.state.leftShortPickerSelectedValue}
              onValueChange={(val,idx)=> {
                //console.log('val='+val,idx)
                this.setState({leftShortPickerSelectedValue:val,leftShortPickerselectedIndex:idx})
                if (idx == 0) {
                  //this.updateCloseShortUI('','','','','','0')
                } else {
                  //console.log(this.state.buyList)
                  let bl = this.state.leftShortList[idx-1]
                  let symbol = bl.symbol
                  let amount = bl.amount
                  let price = bl.price
                  let stop_loss = bl.stop_loss
                  let target_price = bl.target_price
                  let short_id = bl.id
                  ///this.updateCloseShortUI(symbol,amount,price,stop_loss,target_price,short_id)
                }
              }}
            >
              {this.state.leftShortList.map((item, index) => {
                return (<Picker.Item label={item.symbol+' '+item.price+'฿ '+GeneralFunc.convertDT(item.updatedAt)} value={item.id} key={item.id}/>)
              })}
            </Picker>
          </View>

        </View>
        <View style={{marginTop:10}} >
          <Text>Price</Text>
          <TextInput
            style={{width:200, height: 40, borderColor: 'gray', borderWidth: 1 }}
            value={this.state.shortPrice+''}
            onChangeText={(text) => this.setState({shortPrice:text})}
            onFocus={()=>this.setState({shortPrice:''})}
          />
        </View>
        <View style={{marginTop:20,width:100}}>
          <Button style={{}}
            title='Save'
            onPress={()=> {
              if (this.state.sellPrice != 0 &&
                  this.state.sellPickerSelIdx == 0) {
                //create new sell
                this.createSell()
              }

              if (this.state.sellPickerSelIdx != 0) {//update sell
                let price = this.state.sellPrice
                let id = this.state.sellPickerId

                this.updateSell()
              }



            }}
          />
        </View>
      </View>
    )
  }
}
