import React, { Component } from 'react';
import { StyleSheet,View,Image,Text,TextInput,Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {ImageButton} from 'react-native-image-button-text';

export default class MainMenu extends Component {
  constructor() {
    super()
  }

  render() {
    return (
      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <ImageButton width={60} height={60} text='Buy'
          backgroundColor='#0d0' textColor='white' fontSize={25} style={{marginBottom:10}} onPress={() => this.props.navigation.navigate('BuyPanel')}
        />
        <ImageButton width={60} height={60} text='Sell'
          backgroundColor='#0d0' textColor='white' fontSize={25}style={{marginBottom:10}} onPress={() => this.props.navigation.navigate('SellPanel')}
        />

        <ImageButton width={150} height={60} text='Long' backgroundColor='#00e' textColor='white' fontSize={25} style={{marginBottom:10}}/>
        <ImageButton width={150} height={60} text='Close Long' backgroundColor='#00e' textColor='white' fontSize={25} style={{marginBottom:10}}/>

        <ImageButton width={150} height={60} text='Short' backgroundColor='#e00' textColor='white' fontSize={25} style={{marginBottom:10}}/>
        <ImageButton width={150} height={60} text='Close Short' backgroundColor='#e00' textColor='white' fontSize={25} style={{marginBottom:10}}/>
      </View>

    )
  }
}
