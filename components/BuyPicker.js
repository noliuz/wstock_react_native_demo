import React, { Component,useState} from 'react';
import {View,Text,TextInput,Button,Picker } from 'react-native';
import config from '../config.js'
import GeneralFunc from '../GeneralFunc.js'

export default class BuyPicker extends Component {
  state = {
        buyList: [],
        selectedValue:'Create new BUY',
        selectedIndex:'0'
  }

  async componentDidMount() {
    try {
      let url = config.API_URL+'/buy/get/-1'
      console.log(url)
      const buyCall = await fetch(url)
      const buys = await buyCall.json()
      this.setState({buyList: buys})

    } catch (err) {
      console.log("Error fetching data-----------", err);
    }
  }

  render() {
    //console.log(JSON.stringify(this.state.buyList))
    return(
      <View>
        <Picker
          style={{ height: 50, width: 300 }}
          selectedValue={this.state.selectedValue}
          onValueChange={(val,idx)=> {
            //console.log('val='+val,idx)
            this.setState({selectedValue:val,selectedIndex:idx})
            if (idx == 0) {
              this.props.updateBuyState('','','','','','0')
            } else {
              //console.log(this.state.buyList)
              let bl = this.state.buyList[idx-1]
              let symbol = bl.symbol
              let amount = bl.amount
              let price = bl.price
              let stop_loss = bl.stop_loss
              let target_price = bl.target_price
              let buy_id = bl.id
              this.props.updateBuyState(symbol,amount,price,stop_loss,target_price,buy_id)
            }
          }}
        >
          <Picker.Item label="Create new BUY" value={-1} key={-1}/>
          {
            this.state.buyList.map((item, index) => {
              return (
                <Picker.Item label={item.symbol+' '+item.price+'฿ '+GeneralFunc.convertDT(item.updatedAt)} value={item.symbol+' '+item.price+'฿ '+GeneralFunc.convertDT(item.updatedAt)} key={item.id}/>
              )
            })
          }
        </Picker>

      </View>

    )
  }
}
